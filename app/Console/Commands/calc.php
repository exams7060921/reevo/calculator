<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPUnit\Event\InvalidArgumentException;

class calc extends Command
{
    const OPERATOR_ADDITION = '+';
    const OPERATOR_SUBTRACTION = '-';
    const OPERATOR_MULTIPLICATION = '*';
    const OPERATOR_DIVISION = '/';
    const OPERATOR_SQUARE_ROOT = 'sqrt';

    const VALID_OPERATORS = [
        self::OPERATOR_ADDITION,
        self::OPERATOR_SUBTRACTION,
        self::OPERATOR_MULTIPLICATION,
        self::OPERATOR_DIVISION,
        self::OPERATOR_SQUARE_ROOT,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calc {num1} {operator} {num2?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Does basic computation between two numbers';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        try {
            $this->validateArgs(
                $this->argument('num1'),
                $this->argument('operator'),
                $this->argument('num2')
            );
        } catch(\DomainException $e) {
            $this->info($e->getMessage());
            return;
        } catch(\InvalidArgumentException $e) {
            $this->info($e->getMessage());
            return;
        }

        try {
            switch($this->argument('operator')){
                case '+':
                    $result = $this->argument('num1') + $this->argument('num2');
                    break;
                case '-':
                    $result = $this->argument('num1') - $this->argument('num2');
                    break;
                case '*':
                    $result = $this->argument('num1') * $this->argument('num2');
                    break;
                case '/':
                    $result = $this->argument('num1') / $this->argument('num2');
                    break;
                case 'sqrt':
                    $result = sqrt($this->argument('num1'));
                    break;
            }

            $this->info("result: {$result}");
        } catch(\DivisionByZeroError $exception) {
            $this->info('Congratulations you just broke the time-space continuum; You just divided by zero.');
        } catch(\Exception $exception) {
            $this->info('We encountered an error: ' . $exception->getMessage());
        }
    }

    /**
     * @param $num1
     * @param $num2
     * @param $operator
     * @throws InvalidArgumentException
     * @throws \DomainException
     * @return void
     */
    private function validateArgs($num1, $operator, $num2 = null): void
    {
        if (is_numeric($num1) === false) {
            throw new \InvalidArgumentException("{$num1} is not a number");
        }

        if (in_array($operator, self::VALID_OPERATORS) === false) {
            throw new \DomainException("{$operator} is not a valid operator (+, -, *, /)");
        }

        if ($operator !== self::OPERATOR_SQUARE_ROOT &&
            is_numeric($num2) === false
        ) {
            throw new \InvalidArgumentException("{$num2} is not a number");
        }
    }
}
