<p align="center"><a href="https://reevomoney.com" target="_blank"><img src="https://cdn.buttercms.com/Mfn9bzzYRlOHlfUuaR0f" width="100" alt="Rwwvo Logo"></a></p>

## About

This is a Laravel based project for an exam for Reevo. This is a CLI calculator that performs the basic operations:
* addition (+)
* subtraction (-)
* multiplication (*)
* division (/)
* square root (sqrt)

### Usage
use this command
```php artisan calc [num1] [operator] [num2]``` e.g. ```php artisan calc 1 + 1```
