<?php

namespace Tests\Unit\app\Console\Commands;

use App\Console\Commands\calc;
use Tests\TestCase;

/**
 * @covers \App\Console\Commands\calc
 */
class CalcTest extends TestCase
{
    /**
     * @dataProvider handleHappyPathDataProvider
     * @return void
     */
    public function testHandle($num1, $operator, $expected, $num2 = null)
    {
        $this->artisan('calc', [
           'num1' => $num1,
           'operator' => $operator,
           'num2' => $num2,
        ])->expectsOutput("result: {$expected}");
    }

    public static function handleHappyPathDataProvider(): array
    {
        return [
            'should return correct addition result' => [
                1,
                calc::OPERATOR_ADDITION,
                3,
                2,
            ],
            'should return correct subrtraction result' => [
                2,
                calc::OPERATOR_SUBTRACTION,
                1,
                1,
            ],
            'should return correct multiplication result' => [
                1,
                calc::OPERATOR_MULTIPLICATION,
                2,
                2,
            ],
            'should return correct division result' => [
                1,
                calc::OPERATOR_DIVISION,
                0.5,
                2,
            ],
            'should return correct squareroot result' => [
                4,
                calc::OPERATOR_SQUARE_ROOT,
                2
            ],
        ];
    }

    public function testDivideByZero()
    {
        $this->artisan('calc', [
            'num1' => 1,
            'operator' => calc::OPERATOR_DIVISION,
            'num2' => 0,
        ])->expectsOutput("Congratulations you just broke the time-space continuum; You just divided by zero.");
    }

    /**
     * @dataProvider num1IsNotNumericDataProvider
     * @return void
     */
    public function testNum1IsNotNumeric($num1)
    {
        foreach (calc::VALID_OPERATORS as $operator) {
            $this->artisan('calc', [
                'num1' => $num1,
                'operator' => $operator,
                'num2' => 2,
            ])->expectsOutput("{$num1} is not a number");
        }
    }

    public static function num1IsNotNumericDataProvider(): array
    {
        return [
            'should output incorrect parameter message when first parameter is alpha characters' => ['abcdefg'],
            'should output incorrect parameter message when first parameter is alpha-numeric characters' => ['1q2w3e4r5t'],
            'should output incorrect parameter message when first parameter is special-character-numeric characters' => ['*1+2'],
        ];
    }

    /**
     * @dataProvider num2IsNotNumericDataProvider
     * @return void
     */
    public function testNum2IsNotNumeric($num2)
    {
        foreach (calc::VALID_OPERATORS as $operator) {
            if ($operator === calc::OPERATOR_SQUARE_ROOT) {
                continue;
            }

            $this->artisan('calc', [
                'num1' => 2,
                'operator' => $operator,
                'num2' => $num2,
            ])->expectsOutput("{$num2} is not a number");
        }
    }

    public static function num2IsNotNumericDataProvider(): array
    {
        return [
            'should output incorrect parameter message when second number parameter is alpha characters' => ['abcdefg'],
            'should output incorrect parameter message when second number parameter is alpha-numeric characters' => ['1q2w3e4r5t'],
            'should output incorrect parameter message when second number parameter is special-character-numeric characters' => ['*1+2'],
        ];
    }

    /**
     * @dataProvider invalidOperatorDataProvider
     * @return void
     */
    public function testInvalidOperator($operator)
    {
        $this->artisan('calc', [
            'num1' => 2,
            'operator' => $operator,
            'num2' => 2,
        ])->expectsOutput("{$operator} is not a valid operator (+, -, *, /)");
    }

    public static function invalidOperatorDataProvider(): array
    {
        return [
            'should output invalid operator message scenario 1' => ['+*'],
            'should output invalid operator message scenario 2' => ['//'],
            'should output invalid operator message scenario 3' => ['*1+2'],
        ];
    }
}
